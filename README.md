# Two demos for on-device containers

The code in static/ is a simple shell-script that could be a base for a
statically (or partially statically) compiled rust, C++ or C application with
minimal dependencies on the operating system.  For a completely static
application ( no libc dependencies ) one can skip it all and just have a single
binary.

The code in python/ is a more complete Python 3 example targetting
"oldstable" Debian, showing the use of python dependencies, binary
dependencies and the systemd watchdog interactions.
