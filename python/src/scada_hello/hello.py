"""Hello world (complex) demo.

SPDX-License-Identifier:  AGPL-3.0-or-later
"""

import logging
import threading
from typing import Any

from dasbus.connection import MessageBus
from gi.repository import GLib

from .watch import Ticker

LOG = logging.getLogger(__name__)


class HostnamePrinter(Ticker):
    """Gets the hostname from DBus and prints it every few seconds."""

    def __init__(
        self,
        *args: Any,
        shutdown: threading.Event,
        bus: MessageBus,
        **kws: Any,
    ) -> None:
        """Initialize our task"""
        super().__init__(*args, **kws)
        self.shutdown = shutdown
        self.bus = bus
        # Regularly print the hostname
        GLib.timeout_add(7000, self.step)
        GLib.idle_add(self.step_once)

    def step_once(self) -> bool:
        """Called at start-up on idle, runs step and returns false.

        This exists so we can run step without re-scheduling.
        Timer callbacks that return True are re-scheduled, returning False
        or raising an Exception will not re-schedule.
        """
        self.step()
        return False

    def step(self) -> bool:
        """Called every few seconds to do some work."""
        try:
            LOG.debug("Retrieving a dbus-proxy")
            proxy = self.bus.get_proxy(
                "org.freedesktop.hostname1", "/org/freedesktop/hostname1"
            )
            print("Hostname is: ", proxy.Hostname)
        except Exception:  # pylint: disable=broad-exception-caught
            LOG.exception(
                "Failure while printing hostname, shutting down application."
            )
            # This signals that the main loop should terminate because of an
            # unexpected failure
            self.shutdown.set()
            return False
        # This makes the "tick" happen, which causes the counter to increment
        # so the watchdog does not kill our task as one that is inactive.
        return self.tick()


class FailLater(Ticker):
    """A task that fails after a few times of success."""

    def __init__(self, *args: Any, **kws: Any) -> None:
        """Initialize our task"""
        super().__init__(*args, **kws)
        GLib.timeout_add(6000, self.step)
        # Counter, not the same as the tick count to make it clear that we
        # aren't tied together
        self.counter = 0

    def step(self) -> bool:
        """Called every few seconds to do some work."""
        self.counter += 1

        if self.counter < 10:
            LOG.info("Fail Later performed task %s", self.counter)
            return self.tick()

        LOG.error("Something happened and we are not updating tick.")
        return False
