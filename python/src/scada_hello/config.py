"""Hello world (complex) demo.

SPDX-License-Identifier:  AGPL-3.0-or-later
"""

import argparse
import configparser
import logging

LOG = logging.getLogger(__name__)


class Config:
    """Class to encapsulate configuration  and commandline parsing."""

    session = False
    verbose = 0
    config = "/etc/scada-hello.conf"

    def __init__(self) -> None:
        """Does nothing."""

    def parse(self) -> None:
        """Parse commandline and config file."""
        self.parse_cmdline()
        self.parse_config()

    def parse_cmdline(self) -> None:
        """Parse commandline.

        Updates self.
        """
        desc = "Placeholder, please ignore"
        parser = argparse.ArgumentParser(description=desc)
        parser.add_argument(
            "-v",
            "--verbose",
            default=self.verbose,
            action="count",
            help="Add more for increased verbosity",
        )
        parser.add_argument(
            "-s",
            "--session",
            default=self.session,
            action="store_true",
            help="Use session bus (unprivileged)",
        )
        parser.add_argument(
            "-c", "--config", default=self.config, help="config file name"
        )
        args = parser.parse_args()
        self.verbose = args.verbose
        self.session = args.session
        self.config = args.config

    def parse_config(self) -> None:
        """Parse configfile.

        Updates self.
        """
        config = configparser.ConfigParser()
        LOG.info("Reading configuration from: %s", self.config)
        config.read(self.config, encoding="utf8")

        # This is just an example of how to parse a config file.
        # Currently, we only look for a "debug" flag in the "hello" section.
        for section, options in config.items():
            # DEFAULT section should be consumed here,
            # and the per-application section searched for debug flag.
            # All others we just expose further
            if section == "DEFAULT":
                continue

            if section == "hello":
                debug = options.getboolean("debug", False)
                if debug:
                    self.verbose = max(self.verbose, 5)
                continue
