"""Helper for verbose levels.

SPDX-License-Identifier:  AGPL-3.0-or-later
"""

import logging


def verbosity(logger: logging.Logger, verbose: int = 3) -> logging.Logger:
    """Configure a logger based on numerical level."""
    if verbose >= 2:
        logger.setLevel(logging.DEBUG)
    elif verbose >= 1:
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.WARNING)
    return logger
