"""Hello world (complex) demo.

SPDX-License-Identifier:  AGPL-3.0-or-later
"""

import logging
import sys
import threading

from dasbus.connection import SessionMessageBus, SystemMessageBus
from gi.repository import GLib

from . import hello, watch
from .config import Config
from .utils import verbosity

LOG = logging.getLogger("scada_hello")


def shutdown_signal(shutdown: threading.Event, loop: GLib.MainLoop) -> bool:
    """Check if we should shut the main loop down, and do so."""
    # I did not want this in the watchdog code as that may fail in case
    # systemd isn't active or we do not want to do so
    if shutdown.is_set():
        loop.quit()
    return True


def main() -> None:
    """Parse config, set up async, play ball."""
    logging.basicConfig()
    config = Config()
    config.parse()
    verbosity(LOG, config.verbose)
    # Watchdog is spammier than others (on purpouse)
    verbosity(watch.LOG, config.verbose - 1)
    # Other loggers will follow the root one when we change it, so no need to
    # adjust those
    LOG.info(
        "Config is verbosity=%s, file=%s, session=%s",
        config.verbose,
        config.config,
        config.session,
    )

    # If this is set, the threads should shut down (Cleanly)
    shutdown = threading.Event()
    # System / Session is if we work against user or system services.
    # For many times when developing we want to call session (user) services
    # to ensure integration tests work, and so that services won't have to run
    # as root.
    # This means a command line flag is a useful concept.
    bus = SessionMessageBus() if config.session else SystemMessageBus()

    loop = GLib.MainLoop()

    # Set up our two tasks here
    fails_later = hello.FailLater()
    print_task = hello.HostnamePrinter(shutdown=shutdown, bus=bus)
    # Set up the watchdog to be called from systemd
    watch_sock = watch.watchdog_socket()
    # Set up a basic pinger that checks if our tasks make progress, and pings
    # the watchdog if everything seems to work ok.
    watchdog_pinger = watch.make_watchdog(
        watch_sock, shutdown, ticking=[print_task, fails_later]
    )
    GLib.timeout_add(11000, watchdog_pinger)

    # Set up a regular task to check if we _should_ shut down,
    # and in that case signal the event loop to quit.
    GLib.timeout_add(997, shutdown_signal, shutdown, loop)

    try:
        loop.run()
    except KeyboardInterrupt:
        LOG.info("Caught ctrl-c")
        shutdown.set()
        loop.quit()
    except Exception:  # pylint: disable=broad-except
        LOG.exception("loop Exception")
        shutdown.set()
        loop.quit()
    finally:
        LOG.info("Main Loop terminated.")
        shutdown.set()
        loop.quit()

    if shutdown.is_set():
        sys.exit("Terminating all threads")
