"""support for systemd watchdog.

SPDX-License-Identifier:  AGPL-3.0-or-later
"""

import logging
import os
import socket
import threading
from typing import Callable, List, Optional, Tuple

LOG = logging.getLogger(__name__)
Watchdog = Tuple[Optional[str], Optional[socket.socket]]


class Ticker:
    # pylint: disable=too-few-public-methods
    """Tick when something happens.

    Use this as a mix-in for event-driven tasks that should be counted for in
    the watchdog check.
    """

    last_tick: int = 0

    def tick(self) -> bool:
        """Tick the internal counter."""
        self.last_tick += 1
        return True


def watchdog_socket(clean_environment: bool = True) -> Watchdog:
    """Return watchdog address and socket.

    clean_environment removes the variables from env to prevent children
    from inheriting it and doing something wrong.
    """
    _empty = None, None
    address = os.environ.get("NOTIFY_SOCKET", None)
    if clean_environment:
        address = os.environ.pop("NOTIFY_SOCKET", None)

    if not address:
        return _empty

    if len(address) == 1:
        return _empty

    if address[0] not in ("@", "/"):
        return _empty

    if address[0] == "@":
        address = "\0" + address[1:]

    # SOCK_CLOEXEC was added in Python 3.2 and requires Linux >= 2.6.27.
    # It means "close this socket after fork/exec()
    try:
        sock = socket.socket(
            socket.AF_UNIX, socket.SOCK_DGRAM | socket.SOCK_CLOEXEC
        )
    except AttributeError:
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

    return address, sock


def watchdog_ping(
    address: Optional[str], sock: Optional[socket.socket]
) -> bool:
    """Ping the systemd watchdog."""
    message = b"WATCHDOG=1"
    if not (address and sock):
        LOG.debug("No watchdog to ping. Delete this line for production.")
        return False
    try:
        retval = sock.sendto(message, address)
    except socket.error:
        return False

    return retval > 0


def make_watchdog(
    watchdog: Watchdog,
    shutdown: threading.Event,
    ticking: List[Ticker],
) -> Callable[[], bool]:
    """Internalize the arguments to the watchdog so they aren't bound by the
    event loop.

    returns a function that can be passed as a glib callback.
    """
    prev = [t.last_tick for t in ticking]

    def watchdog_pinger() -> bool:
        """Regular callback to ping the watchdog."""
        nonlocal prev
        for i, t in enumerate(ticking):
            last = t.last_tick
            if prev[i] == last:
                LOG.error("Watchdog: %s is not ticking, last=%s", t, last)
                shutdown.set()
            else:
                prev[i] = last

        if not shutdown.is_set():
            LOG.info("Pinging watchdog. Delete this line for production.")
            watchdog_ping(*watchdog)
        else:
            LOG.error("Watchdog: Shutdown flagged, shutting down.")
        # Always reschedule
        return True

    return watchdog_pinger
