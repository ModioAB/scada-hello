"""Placeholder init.

SPDX-License-Identifier:  AGPL-3.0-or-later
"""

from .app import main

if __name__ == "__main__":
    main()
