# Python hello world

This is a non-trivial Python example, using external dependencies (python3-gi,
dasbus), a package build, ( First compile a wheel, then install ) and more.

The Makefile is fairly simple, setting up the files and paths to add to the
Dockerfile during build.

There is a commented out example for how to do package builds outside of the
main container build, which is how I would _recommend_ using it, but doing it
that way makes the example harder to understand when only looking at the
Dockerfile.


# The application

The application follows a pattern we use internally, one Config object
responsible for parsing command line as well as config-file data into a python
class.

Then there is Logging setup, mainloop, watchdog integration, and a few tasks to
show how the various interactions may work.

There is no _need_ to use this kind of setup, but it's been working in
practice.

One sub-task of the application will talk on DBus and print the hostname every few seconds. Another will succeed the first few calls and then fail.

The watchdog integration checks that all tasks are making progress between
calls, and if that fails, will shut down and no longer ping the external
watchdog.
